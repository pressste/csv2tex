# csv2tex
## A python script that converts a file with separators into a LaTeX table
Nothing to install, just launch ./csv2tex (I've added this folder to path so I don't have to type python etc etc)
## Arguments
### -i (Name of the input file)
Usage: -i file_name  
Example: csv2tex -i test.txt  
If the file provided via -i does not exist, the program quits.  
If no input file is specified, programs asks the user to input a file. If the file does not exist, program ends.  

### -d (Delimeter used)
Usage: -d delimeter  
Example: -d ,  
The delimeter is then used in function split.  
If no delimeter is provided, it defaults to ","  

### -o (Output file)
Usage: -o file_name  
Example: -o voltage_table.txt   
If no output file is specified, it defaults to "table.txt"  